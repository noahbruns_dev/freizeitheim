<div class="pt-5 pb-3 bg-white">
  <div class="container">
    <h1 class="heading text-center mb-5" id="umgebung">Umgebung</h1>
    <section class="slider row" id="slider">
      <div>
        <div class="card-image">
          <img class="card-img-top" hieght="1200px" src="images/gleinkersee.jpg">
          <div class="card-lower text-center">
            <h5 class="card-title">Gleinkersee</h5>
            <a href="http://www.gleinkersee.at/" class="btn btn-primary" target="_blank">Mehr</a>
          </div>
        </div>
      </div>
      <div>
        <div class="card-image">
          <img class="card-img-top" hieght="1200px" src="images/lebenextrem.jpg">
          <div class="card-lower text-center">
            <h5 class="card-title">lebenextrem</h5>
            <a href="http://www.lebenextrem.at/" class="btn btn-primary" target="_blank">Mehr</a>
          </div>
        </div>
      </div>
      <div>
        <div class="card-image">
          <img class="card-img-top" hieght="1200px" src="images/klamm.jpg">
          <div class="card-lower text-center">
            <h5 class="card-title">Dr. Vogelgesang-Klamm</h5>
            <a href="https://www.urlaubsregion-pyhrn-priel.at/oesterreich/tour/430000994/dr-vogelgesang-klamm.html?h=1889" class="btn btn-primary" target="_blank">Mehr</a>
          </div>
        </div>
      </div>
      <div>
        <div class="card-image">
          <img class="card-img-top" hieght="1200px" src="images/wurzeralm.jpg">
          <div class="card-lower text-center">
            <h5 class="card-title">Wurzeralm / Hinterstoder</h5>
            <a href="http://www.hiwu.at/" class="btn btn-primary" target="_blank">Mehr</a>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {
    $("#slider").slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      responsive: [
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true
          }
        }
      ]
    });
  });
</script>