<div id="carousel-main" class="carousel slide" data-ride="carousel" data-interval="8000">
  <ol class="carousel-indicators">
    <li data-target="#carousel-main" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-main" data-slide-to="1"></li>
    <li data-target="#carousel-main" data-slide-to="2"></li>
    <li data-target="#carousel-main" data-slide-to="3"></li>
    <li data-target="#carousel-main" data-slide-to="4"></li>
    <li data-target="#carousel-main" data-slide-to="5"></li>
    <li data-target="#carousel-main" data-slide-to="6"></li>
    <li data-target="#carousel-main" data-slide-to="7"></li>
  </ol>

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slide1.jpg" class="d-block w-100">
      <div class="mycarousel-caption white c-leftbottom">
        <h3>Herzlich Willkommen!</h3>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide2.jpg" class="d-block w-100">
      <div class="mycarousel-caption white c-leftbottom">
        <h3>Gemeinschaftsraum</h3>
        <p>Wir vermieten Räumlichkeiten für Gruppen</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide3.jpg" class="d-block w-100">
      <div class="mycarousel-caption black c-leftbottom">
        <h3>Küche</h3>
        <p>Mit allem benötigten für Selbstversorger</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide4.jpg" class="d-block w-100">
      <div class="mycarousel-caption black c-righttop">
        <h3>Zimmer</h3>
        <p>insgesamt 33 Betten</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide5.jpg" class="d-block w-100">
      <div class="mycarousel-caption black c-righttop">
        <h3>Zimmer</h3>
        <p>2-5 - Betten pro Zimmer</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide6.jpg" class="d-block w-100">
      <div class="mycarousel-caption white c-leftbottom">
        <h3>Aktivitäten</h3>
        <p>Wuzzler und Tischtennis Räume</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide7.jpg" class="d-block w-100">
      <div class="mycarousel-caption white c-leftbottom">
        <h3>Gemeinschaftsraum</h3>
        <p>Neuer Kirchenraum mit x Plätzen</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="images/slide8.jpg" class="d-block w-100">
      <div class="mycarousel-caption white c-leftbottom">
        <h3>Kommt uns Besuchen</h3>
        <p>im wunderschönen Windischgarsten</p>
      </div>
    </div>
  </div>

  <a class="carousel-control-prev" href="#carousel-main" role="button" data-slide="prev">
    <i class="icon-left-open-big" style="font-size: 3em;"></i>
    <span class="sr-only">Previous</span>
  </a>

  <a class="carousel-control-next" href="#carousel-main" role="button" data-slide="next">
    <i class="icon-right-open-big" style="font-size: 3em;"></i>
    <span class="sr-only">Next</span>
  </a>
</div>