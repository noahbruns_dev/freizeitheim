<?
// logging page hits
$dbfolder = $_SERVER["DOCUMENT_ROOT"]."/data/";
$dbname = $_SERVER["HTTP_HOST"]."_log.sq3";

// check if database file exists first
if(!file_exists($dbfolder.$dbname))
{
 $logdb = new PDO("sqlite:".$dbfolder.$dbname);
 $logdb->exec("CREATE TABLE hits(page VARCHAR(255) PRIMARY KEY, counter INTEGER)");
}
else
{
 $logdb = new PDO("sqlite:".$dbfolder.$dbname);
}

$page = $_SERVER["SCRIPT_URL"];

// check if page is already in the hits table
$statement = $logdb->query("SELECT counter FROM hits WHERE page='$page'");
$record = $statement->fetchAll();

// if a record is found
if(sizeof($record) != 0)
{
 $counter = $record[0]['counter']++;
 $logdb->exec("UPDATE hits SET counter=$counter WHERE page='$page'");
}
else
{
 $logdb->exec("INSERT INTO hits(page, counter) VALUES ('$page', 1)");
}

// close connection
$logdb = null;
?>
