<div class="p-3 pt-5 bg-dark">
	<div class="container text-white">
		<div class="row">
			<div class="col-md-6 text-center text-light">
				<h4>KONTAKT</h4>
				<p>Evangelische Tochtergemeinde Windischgarsten<br>
				Römerweg 7<br>
				4580 Windischgarsten<br>
				Telefon: <a class="text-info" href="tel:+437562 5219">+43 (7562) 5219</a><br>
				Mobil:&nbsp; <a class="text-info" href="tel:+4369918877495">+43 (699) 188 77 495</a></p>
				<p>
					<button type="button" class="btn text-white btn-primary" data-toggle="modal" data-target="#Impressum">
						IMPRESSUM
					</button>
					<button type="button" class="btn text-white btn-primary" data-toggle="modal" data-target="#Datenschutz">
						DATENSCHUTZERKLÄRUNG
					</button>
				</p>
			</div>
			<div class="col-md-6 text-center text-light">
				<div class="mb-3"><a href="https://evang-windischgarsten.at/" target="_blank"><img src="images/EVG_logo.svg" height="90px"></a></div>
				<div>
					<p class="text-light">Mitglied von:<p>
					<a href="https://vch.at/de/home/" target="_blank"><img src="images/vch.jpg" height="60px" class="mr-3"></a>
					<a href="https://www.urlaubsregion-pyhrn-priel.at/" target="_blank"><img src="images/pyhrnpriel.png" height="60px"></a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Impressum -->
<div class="modal fade" id="Impressum" tabindex="-1" role="dialog" aria-labelledby="modal-impressum" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-impressum">Impressum</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><h5 class="mt-3 mb-1">Medieninhaber und Betreiber:</h5>
		Evangelische Tochtergemeinde Windischgarsten<br>
		Römerweg 7<br>
		4580 Windischgarsten<br>
		Telefon: +43 (7562) 5219<br>
		E-Mail: <a href="mailto:office@evang-windischgarsten.at">office@evang-windischgarsten.at</a></p>
		<p><h5 class="mt-3 mb-1">Vertreten durch:</h5>
		Kurator Dr. Reinhard Füßl<br>
		E-Mail: <a href="mailto:kurator@evang-windischgarsten.at">kurator@evang-windischgarsten.at</a></p>
		<p><h5 class="mt-3 mb-1">Grundlegende Richtung:</h5>
		Information über Themen aus dem Bereich des Freizeitheims der Evangelischen Tochtergemeinde Windischgarsten.</p>
		<p><h5 class="mt-3 mb-1">Bildnachweis:</h5>
		Fotos von Gemeindemitgliedern und Fotos der Webseite <a href="http://www.pixabay.com" target="_blank" rel="noopener">www.pixabay.com</a> (CC0 Creative Commons Lizenz)</p>
		<p>Informationen über die Online-Beilegung verbraucherrechtlicher Streitigkeiten unter <a href="http://ec.europa.eu/consumers/odr/" target="_blank" rel="noopener">http://ec.europa.eu/consumers/odr/</a></p>

		Bild zur Wurzeralm:<br>
		Foto Copytight Bruno Sulzbacher<br>
		Link: <a href="http://www.hiwu.at/wurzeralm/de/sommer/bergsommer/bildergalerie" target="_blank">hiwu.at</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Datenschutz -->
<div class="modal fade" id="Datenschutz" tabindex="-1" role="dialog" aria-labelledby="modal-datenschutz" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-datenschutz">Erklärung zum Datenschutz (Privacy Policy)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3 class="mt-2 mb-3">Vorwort</h3>
		<p>Die Betreiber dieser Web-Seiten nehmen den Schutz privater Daten ernst. Die besondere Beachtung der Privatsphäre bei der Verarbeitung persönlicher Daten ist ein wichtiges Anliegen. Persönliche Daten werden gemäss den Bestimmungen des Datenschutzgesetzes verwendet; die Betreiber dieser Web-Seiten verpflichten sich zur Verschwiegenheit. Diese Web-Seiten können Links zu Web-Seiten anderer AnbieterInnen enthalten, auf die sich diese Datenschutzerklärung nicht erstreckt. Weitere wichtige Informationen finden sich auch in den Allgemeinen Nutzungsbedingungen.</p>

		<h3 class="mt-4 mb-3">1. Personenbezogene Daten</h3>
		<p>Personenbezogene Daten sind Informationen, die dazu genutzt werden können, die Identität der jeweiligen Person zu erfahren. Darunter fallen Informationen wie Name, Adresse, Postanschrift, Telefonnummer. Informationen, die nicht direkt mit der Identität einer Person in Verbindung gebracht werden (wie zum Beispiel favorisierte Web-Seiten oder die Anzahl der NutzerInnen eines bestimmten Internet-Angebots) fallen nicht darunter.</p>

		<p>Man kann dieses Online-Angebot grundsätzlich ohne Offenlegung der Identität nutzen. Wenn man sich für eine Registrierung entscheidet, sich also als Mitglied (registrierter Benutzer bzw. registrierte Benutzerin) anmeldet, kann man im individuellen Benutzungsprofil persönliche Informationen angeben. Es unterliegt der freien Entscheidung jedes Mitglieds, ob diese Daten angegeben werden. Da seitens der Betreiber versucht wird, für die Nutzung des Angebots so wenig personenbezogene Daten wie möglich zu erheben, reicht für eine Registrierung die Angabe des Namens – unter dem man als Mitglied geführt wird und der nicht mit dem realen Namen übereinstimmen muss – und die Angabe der E-Mail-Adresse, an die das Kennwort geschickt wird, aus. In Verbindung mit dem Zugriff auf die Web-Seiten werden serverseitig Daten (zum Beispiel IP-Adresse, Datum, Uhrzeit und betrachtete Seiten) gespeichert. Es findet keine personenbezogene Verwertung statt. Die statistische Auswertung anonymisierter Datensätze bleibt vorbehalten.</p>

		<p>Wir nutzen die persönlichen Daten zu Zwecken der technischen Administration der Web-Seiten und zur Mitgliederverwaltung nur im jeweils dafür erforderlichen Umfang. Darüber hinaus werden persönliche Daten nur dann gespeichert, wenn diese freiwillig angegeben werden.</p>

		<h3 class="mt-4 mb-3">2. Weitergabe personenbezogener Daten</h3>
		<p>Wir verwenden personenbezogene Informationen nur für diese Web-Seiten. Wir geben die Informationen ohne Ihr ausdrückliches Einverständnis nicht an Dritte weiter. Sollten im Rahmen einer Auftragsdatenverarbeitung Daten an Dienstleister weitergegeben werden müssen, so sind diese Dienstleister an das Datenschutzgesetz, andere gesetzliche Vorschriften und an diese Erklärung zum Datenschutz gebunden.</p>

		<p>Erhebungen beziehungsweise Übermittlungen persönlicher Daten an staatliche Einrichtungen und Behörden erfolgen nur im Rahmen zwingender Rechtsvorschriften.</p>

		<h3 class="mt-4 mb-3">3. Einsatz von Cookies</h3>
		<p>Auf diesen Web-Seiten werden Cookies eingesetzt, kleine Dateien mit Konfigurationsinformationen. Sie helfen, individuelle Einstellungen zu ermitteln und spezielle Funktionen für die BenutzerInnen zu realisieren. Die Betreiber erfassen keine personenbezogenen Daten über Cookies. Sämtliche Funktionen dieser Web-Seiten sind auch ohne Cookies einsetzbar, einige spezielle Eigenschaften und Einstellungen sind dann allerdings nicht verfügbar.</p>

		<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sogenannte „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt.</p>

		<p>Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inklusive Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href="https://tools.google.com/dlpage/gaoptout?hl=de">tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>

		<p>Weitere Informationen zu Google Analytics betreffend Nutzungsbedingungen und Datenschutz finden Sie unter <a href="https://www.google.com/analytics/terms/de.html">www.google.com/analytics/terms/de.html</a>. Auf dieser Website wird Google Analytics um den Code „anonymizeIp: true“ erweitert verwendet, um die anonymisierte Erfassung von IP-Adressen zu gewährleisten.</p>

		<h3 class="mt-4 mb-3">4. Minderjährige</h3>
		<p>Personen unter 18 Jahren sollten ohne Zustimmung der Eltern oder Erziehungsberechtigten im Rahmen dieser Web-Seiten keine personenbezogenen Daten übermitteln. Die Betreiber fordern keine personenbezogenen Daten von Minderjährigen an, sammeln diese nicht und geben sie nicht an Dritte weiter.</p>

		<h3 class="mt-4 mb-3">5. Recht auf Widerruf</h3>
		<p>Wenn Sie auf diesen Web-Seiten personenbezogene Daten angegeben haben, können Sie diese jederzeit wieder ändern und löschen. Nähere Informationen dazu entnehmen Sie bitte den erklärenden Informationen zur jeweiligen Anwendung (bspw. dem Menüpunkt „Hilfe“). Für eine vollständige Löschung des Accounts wenden Sie sich bitte an den <a href="mailto:office@evang-windischgarsten.at">Webmaster</a>. Ihre hier veröffentlichten Artikel, Kommentare und sonstigen Beiträge können auch bei einer vollständigen Löschung Ihres Accounts erhalten bleiben. Weitere Informationen dazu finden Sie bei den Allgemeinen Nutzungsbedingungen.</p>

		<h3 class="mt-4 mb-3">6. Links zu anderen Websites</h3>
		<p>Dieses Online-Angebot enthält Verweise (Links) zu anderen, externen Web-Seiten. Die Betreiber haben keinen Einfluss darauf, ob die Datenschutzbestimmungen auf den anderen, externen Web-Seiten eingehalten werden.</p>

		<h3 class="mt-4 mb-3">7. Beiträge</h3>
		<p>Die Beiträge auf diesen Web-Seiten sind prinzipiell für jeden zugänglich. Beiträge sollten deshalb vor der Veröffentlichung durch den Benutzer bzw. die Benutzerin sorgfältig darauf überprüft werden, ob sie Angaben enthalten, die nicht für die Öffentlichkeit bestimmt sind. Die Beiträge werden möglicherweise in Suchmaschinen erfasst und können auch ohne gezielten Aufruf dieser Web-Seiten weltweit verfügbar sein.</p>

		<h3 class="mt-4 mb-3">8. Fragen und Kommentare</h3>
		<p>Bei Fragen oder für Anregungen und Kommentare zum Thema Datenschutz wenden Sie sich bitte per E-Mail an den <a href="mailto:office@evang-windischgarsten.at">Webmaster</a>.</p>

		<p class="mt-4">Kontaktinformationen zu Medieninhaber & Herausgeber finden Sie im Impressum.</p>
      </div>
    </div>
  </div>
</div>