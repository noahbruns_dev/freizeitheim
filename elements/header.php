<nav class="navbar navbar-expand-md sticky-top bg-white pt-0 pb-0">
  <a class="navbar-brand m-0 pt-1 pb-1 pl-3 pr-3" href="#">
    <img src="images/logo.svg" width="80" height="80" class="d-inline-block align-top" alt="">
  </a>
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar_content" aria-controls="navbar_content" aria-expanded="false" aria-label="Toggle navigation">
    <span>MENÜ</span> <i class="far fa-minus-square"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbar_content">
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="#whoweare">Home</a>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link" href="#räume">Räume</a>
      </li>-->
      <li class="nav-item">
        <a class="nav-link" href="#umgebung">Umgebung</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#anfahrt">Anfahrt</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#kontakt">Preise & Anfrage</a>
      </li>
    </ul>
  </div>
</nav>