<div class="pt-5 pb-3 bg-white">
  <div class="container">
    <h1 class="heading text-center mb-4" id="kontakt">Preise & Anfrage</h1>
    <div class="row">
      <div class="p-2 col-md-3">
        <div class="card box-shadow bg-secondary text-white">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Erwachsene <small class="text-light">ab 16 Jahre</small></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><b>16,00 €</b> <br><small class="text-light"> + 2,00 € Kurtaxe / Nacht</small></h1>
          </div>
        </div>
      </div>
      <div class="p-2 col-md-3">
        <div class="card box-shadow bg-secondary text-white">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Jugendliche <small class="text-light">6-15 Jahre</small></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><b>15,00 €</b> <small class="text-light"> / Nacht</small></h1>
          </div>
        </div>
      </div>
      <div class="p-2 col-md-3">
        <div class="card box-shadow bg-secondary text-white">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Kind <small class="text-light">3-5 Jahre</small></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><b>6,00 €</b> <small class="text-light"> / Nacht</small></h1>
          </div>
        </div>
      </div>
      <div class="p-2 col-md-3">
        <div class="card box-shadow bg-secondary text-white">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Kind <small class="text-light">0-2 Jahre</small></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><b>0,00 €</b> <small class="text-light"> / Nacht</small></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-4 mb-3">
      <div class="col-md-9">
        <h5><b class="text-danger">Neu:</b> incl. Pyhrn Priel Card für die Monate - Ende April - Ende Oktober</h5>
      </div>
    </div>
    <div class="row mt-4 mb-3">
      <div class="col-md-2">
        <img src="images/loesch_hubert.jpg" width="100%">
      </div>
      <div class="col-md-4 pl-3">
        <h4>Leitung: Diakon Hubert Lösch</h4>
        <ul>
          <li>E-Mail: <a href = "mailto:info@freizeitheim.at">info@freizeitheim.at</a></li>
          <li>Telefon: <a href = "tel:+4369918877495">+43 699 188 77 495</a></li>
        </ul>
      </div>
      <div class="col-md-3 p-4 offset-md-3">
        <img src="images/logo.svg" width="100%">
      </div>
    </div>
  </div>
</div>