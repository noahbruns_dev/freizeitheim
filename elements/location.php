<div class="pt-5 pb-3 bg-white">
  <div class="container">
  	<h1 class="heading text-center mb-4" id="anfahrt">Anfahrt</h1>
  	<div class="row">
  		<div class="col-md-4 text-center">
        <h4 class="mb-4">Römerweg 7,<br>
        4580 Windischgarsten</h4>

        <h5 class="mt-4">Mit der Bahn:</h5>
        <p>
          Bahnhof Windischgarsten (15min zu Fuß)<br>
          Fahrplan: <a href="https://tickets.oebb.at" target="about:blank">tickets.oebb.at</a>
        </p>
        <h5 class="mt-4">Mit dem Auto:</h5>
        <p>
          Windischgarsten liegt an der A9 Pyhrn Autobahn
          vom Voralpenkreuz in Sattledt Richtung Graz/Slowenien<br>
          Autobahnabfahrt Roßleithen/Windischgarsten
        </p>
  		</div>
  		<div class="col-md-8">
  			<iframe class="w-100" style="min-height: 400px;" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJtTZi5pd7cUcRgVRnbqcaIaQ&key=AIzaSyDwjhiT12gsOGebr3wSMIHf-CaXL3q7t8c" allowfullscreen></iframe>
  		</div>
  	</div>
  </div>
</div>