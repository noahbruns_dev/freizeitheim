<div class="pt-5 pb-3 bg-white">
  <div class="container">
    <h1 class="heading text-center mb-4" id="whoweare">Was wir bieten</h1>
    <div class="row">
      <div class="col-md-6 text-center pb-3">
        <h4>Herzlich Willkommen</h4> <p> auf der Homepage des Evangelischen Freizeitheims  Windischgarsten.</p>

		<p>Unser Freizeitheim liegt inmitten des Windischgarstnertales, in der Ferienregion Pyhrn-Priel, im südlichen Oberösterreich. Umgeben von einer wunderbaren Bergkulisse lädt es zu einem Urlaub mit vielen Freizeitaktivitäten ein. Das ganze Jahr über bietet sich unsere Naturlandschaft zum Bergwandern, Klettern, Erforschen von Höhlen, Radfahren, und Skifahren in zwei Skigebieten an. Die Langlaufloipe ist ganz in der Nähe, ebenso das öffentliche Hallenbad.</p>

		<p>Unser Freizeitheim verfügt über einen großen Garten mit schöner Aussicht, der zum Verweilen einlädt. Er kann aber auch zum Fußball- und Volleyballspielen benützt werden und weist eine Feuerstelle auf, die gerne in Anspruch genommen wird.</p>

		<p>Wir freuen uns auf ihre Anfrage und stehen für weitere Auskünfte gern zur Verfügung</p>
      </div>
      <div class="col-md-5 offset-md-1 text-left">
        <h4>Ausstattung:</h4>
        <ul>
        	<li>2-5 - Bettzimmer - 33 Betten</li>
        	<li>8 Schlafräume mit Waschbecken</li>
        	<li>Duschen und WC am Gang</li>
        	<li>1 Speiseraum</li>
        	<li>Gruppenräume</li>
        	<li>Der neue Begegnungsraum (120m², siehe <a href="https://zubau.at" target="about:blank">zubau.at</a>) kann von unseren Gruppen gemietet werden.</li>
        	<li>Ein großer Garten mit Spiel- und Volleyballplatz, Boulderwand und Niedrigseilelementen bietet viele Möglichkeiten für gemeinsame Aktivitäten.</li>
        	<li>Tischfußball und Tischtennis sind vorhanden.</li>
        </ul>
      </div>
    </div>
  </div>
</div>