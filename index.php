<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Costom -->
    <link rel="stylesheet" type="text/css" href="main.css">

    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-material/css/bootstrap-material-design.css" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Roboto -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">

    <!-- Slick -->
    <link rel="stylesheet" type="text/css" href="slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css">

    <!-- Entypo -->
    <link rel="stylesheet" href="entypo/entypo.css" crossorigin="anonymous">

    <link rel="canonical" href="https://freizeitheim.at/" />

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
    <script src="bootstrap-material/js/bootstrap-material-design.min.js" crossorigin="anonymous"></script>
    <script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6LdE3tsUAAAAAHFFyICwZVP8fYXH7rLVmcwLbVNT"></script>

    <title>Freizeitheim Windischgarsten</title>
  </head>
  <body data-spy="scroll" data-target=".navbar" data-offset="0">
    <!-- Header -->
    <?php include("elements/header.php"); ?>

    <!-- Carousel-->
    <?php include("elements/carousel.php"); ?>
      <!-- WhoWeAre -->
      <?php include("elements/whoweare.php"); ?>
      <!-- Rooms -->
      <?php include("elements/rooms.php"); ?>
      <!-- Aktivitäten -->
      <?php include("elements/activities.php"); ?>
      <!-- Location -->
      <?php include("elements/location.php"); ?>
      <!-- Kontakt -->
      <?php include("elements/kontakt.php"); ?>

    <!-- Footer -->
    <?php include("elements/footer.php"); ?>

    <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
  </body>
</html>